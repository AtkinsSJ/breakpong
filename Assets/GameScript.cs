﻿using UnityEngine;
using System.Collections;

public class GameScript : MonoBehaviour {

	private int leftScore, rightScore;
	public ScoreScript leftScoreDisplay, rightScoreDisplay;

	public GameObject menu;
	public GameObject leftWins, rightWins;

	private const int SCORE_TO_WIN = 10;

	public BallScript ball;
	public PaddleScript leftPaddle, rightPaddle;

	public AudioClip playButtonSound;

	void Awake() {
		ball.game = this;
	}

	// Use this for initialization
	public void StartGame() {
		leftScore = 0;
		rightScore = 0;

		leftScoreDisplay.SetScore(leftScore);
		rightScoreDisplay.SetScore(rightScore);

		leftPaddle.OnGameStart();
		rightPaddle.OnGameStart();
		ball.OnGameStart();

		AudioSource.PlayClipAtPoint(playButtonSound, transform.position);

		menu.SetActive(false);

		StartRound();
	}

	private void StartRound() {
		ball.Restart();
	}

	public void LeftPlayerScored() {
		leftScore++;
		if (leftScore >= SCORE_TO_WIN) {
			GameOver(true);
		} else {
			leftScoreDisplay.SetScore(leftScore);
			StartRound();
		}
	}

	public void RightPlayerScored() {
		rightScore++;
		if (rightScore >= SCORE_TO_WIN) {
			GameOver(false);
		} else {
			rightScoreDisplay.SetScore(rightScore);
			StartRound();
		}
	}

	private void GameOver(bool leftWon) {

		leftWins.SetActive(leftWon);
		rightWins.SetActive(!leftWon);

		leftPaddle.OnGameOver();
		rightPaddle.OnGameOver();
		ball.OnGameOver();
		menu.SetActive(true);
	}
}
