﻿using UnityEngine;
using System.Collections;

public class PaddleScript : MonoBehaviour {

	public GameObject pixelPrefab;
	private GameObject[,] pixels;

	private const int WIDTH = 2,
						HEIGHT = 8;
	private const float MAX_Y = 23f;
	//private const float MAX_Y = 23f - (HEIGHT / 2f),
	//					MIN_Y = -MAX_Y;
	public string controlAxis = "player1";
	private const float speed = 30f;
	private int topHeight, bottomHeight;

	private bool lostPixelThisFrame = false;
	private Vector3 startPosition;

	// Use this for initialization
	void Start() {
		startPosition = transform.position;
		pixels = new GameObject[HEIGHT, WIDTH];
		Vector3 offset = new Vector3(-WIDTH / 2f + 0.5f, -HEIGHT / 2f + 0.5f);
		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				GameObject pixel = Instantiate(pixelPrefab);
				pixel.transform.SetParent(transform, false);
				pixel.transform.localPosition = offset + new Vector3(x, y, 0);

				pixel.GetComponent<PaddlePixelScript>().Init(x, y, this);

				pixels[y, x] = pixel;
			}
		}

		topHeight = HEIGHT / 2;
		bottomHeight = HEIGHT / 2;

		gameObject.SetActive(false);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		Gizmos.DrawCube(transform.position, new Vector3(WIDTH, HEIGHT, 1));
	}

	// Update is called once per frame
	void Update() {
		Vector3 position = transform.position;
		position.y += Input.GetAxisRaw(controlAxis) * Time.deltaTime * speed;
		float maxY = MAX_Y - topHeight,
			minY = -MAX_Y + bottomHeight;
		position.y = Mathf.Clamp(position.y, minY, maxY);

		transform.position = position;

		lostPixelThisFrame = false;
	}

	internal void OnPixelHit(PaddlePixelScript pixel, int x, int y) {
		if (!lostPixelThisFrame) {
			pixel.gameObject.SetActive(false);
			lostPixelThisFrame = true;

			// Recalculate top and bottom height

			// topHeight
			{
				bool foundPixel = false;
				for (int i = 0; i < HEIGHT; i++) {
					int yy = HEIGHT - (1 + i);

					for (int xx = 0; xx < WIDTH; xx++) {
						if (pixels[yy, xx].activeSelf) {
							foundPixel = true;
							break;
						}
					}
					if (foundPixel) {
						topHeight = yy + 1 - HEIGHT / 2;
						break;
					}
				}
				if (!foundPixel) {
					topHeight = 0;
				}
				Debug.Log("topHeight:" + topHeight);
			}

			// bottomHeight
			{
				bool foundPixel = false;
				for (int i = 0; i < HEIGHT; i++) {
					int yy = i;
					for (int xx = 0; xx < WIDTH; xx++) {
						if (pixels[yy, xx].activeSelf) {
							foundPixel = true;
							break;
						}
					}
					if (foundPixel) {
						bottomHeight = HEIGHT / 2 - i;
						break;
					}
				}
				if (!foundPixel) {
					bottomHeight = 0;
				}
				Debug.Log("bottomHeight:" + bottomHeight);
			}

			if (topHeight + bottomHeight == 0) {
				// Whole bat is gone! Regenerate it!
				StartCoroutine(FillPaddle());
			}
		}
	}

	IEnumerator FillPaddle() {
		yield return new WaitForSeconds(0.3f); // Let the ball move out of the paddle first

		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				pixels[y, x].SetActive(true);
			}
		}

		topHeight = HEIGHT / 2;
		bottomHeight = HEIGHT / 2;
	}

	public void OnGameStart() {
		transform.position = startPosition;

		FillPaddle();

		gameObject.SetActive(true);
	}

	public void OnGameOver() {
		gameObject.SetActive(false);
	}
}
