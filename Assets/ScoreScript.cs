﻿using UnityEngine;
using System.Collections;

// This is a grid of Quads, which will display the scores
public class ScoreScript : MonoBehaviour {

	private const int GRID_HEIGHT = 5,
						GRID_WIDTH = 3;

	private readonly int[,,] NUMBERS = {
		{
			{0,1,0},
			{1,0,1},
			{1,0,1},
			{1,0,1},
			{0,1,0},
		},
		{
			{0,1,0},
			{1,1,0},
			{0,1,0},
			{0,1,0},
			{1,1,1},
		},
		{
			{0,1,0},
			{1,0,1},
			{0,0,1},
			{0,1,0},
			{1,1,1},
		},
		{
			{1,1,0},
			{0,0,1},
			{1,1,0},
			{0,0,1},
			{1,1,0},
		},
		{
			{1,0,1},
			{1,0,1},
			{1,1,1},
			{0,0,1},
			{0,0,1},
		},
		{
			{1,1,1},
			{1,0,0},
			{1,1,0},
			{0,0,1},
			{1,1,0},
		},
		{
			{0,1,1},
			{1,0,0},
			{1,1,0},
			{1,0,1},
			{0,1,0},
		},
		{
			{1,1,1},
			{0,0,1},
			{0,0,1},
			{0,1,0},
			{0,1,0},
		},
		{
			{0,1,0},
			{1,0,1},
			{0,1,0},
			{1,0,1},
			{0,1,0},
		},
		{
			{0,1,0},
			{1,0,1},
			{0,1,1},
			{0,0,1},
			{1,1,0},
		},
	};

	public GameObject pixelPrefab;

	private int score = -1;

	private GameObject[,] pixels;

	// Use this for initialization
	void Start() {
		generateScoreGrid();
		SetScore(0);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawCube(transform.position, new Vector3(GRID_WIDTH, GRID_HEIGHT, 1));
	}

	private void generateScoreGrid() {
		pixels = new GameObject[GRID_HEIGHT, GRID_WIDTH];
		Vector3 offset = new Vector3(-GRID_WIDTH/2f+0.5f, -GRID_HEIGHT/2f+0.5f, 0);

		for (int y = 0; y < GRID_HEIGHT; y++) {
			for (int x = 0; x < GRID_WIDTH; x++) {
				GameObject pixel = Instantiate(pixelPrefab);
				pixel.transform.SetParent(transform, false);
				pixel.transform.localPosition = offset + new Vector3(x, y, 0);

				pixels[y, x] = pixel;
			}
		}
	}

	public void SetScore(int score) {
		if (this.score == score) return;
		if (score < 0 || score > 9) {
			throw new System.Exception("Only scores from 0 to 9 inclusive are supported!");
		}

		this.score = score;

		for (int y = 0; y < GRID_HEIGHT; y++) {
			int iy = GRID_HEIGHT - 1 - y;
			for (int x = 0; x < GRID_WIDTH; x++) {
				pixels[y, x].SetActive(
					NUMBERS[score,iy,x] == 1
				);
			}
		}
	}
}
