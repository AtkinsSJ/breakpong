﻿using UnityEngine;
using System.Collections;

public class LineScript : MonoBehaviour {

	public GameObject pixelPrefab;

	public int length;
	public bool dashed;
	public int dashLength;

	// Use this for initialization
	void Start () {
		float startX = -length / 2f + 0.5f;
		if (dashed) {
			for (int x = 0; x < length; x+= dashLength*2) {
				for (int i = 0; i < dashLength; i++) {
					GameObject pixel = Instantiate(pixelPrefab);
					pixel.transform.SetParent(transform, false);
					pixel.transform.localPosition = new Vector3(startX + x + i, 0, 0);
				}
				
			}
		} else {
			for (int x = 0; x < length; x++) {
				GameObject pixel = Instantiate(pixelPrefab);
				pixel.transform.SetParent(transform, false);
				pixel.transform.localPosition = new Vector3(startX + x, 0, 0);
			}
		}
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.white;
		Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
		Gizmos.matrix = rotationMatrix;

		Gizmos.DrawCube(transform.position, new Vector3(length, 1, 1));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
