﻿using UnityEngine;
using System.Collections;

public class PaddlePixelScript : MonoBehaviour {

	private int x, y; // Within the paddle
	private PaddleScript paddle;

	internal void Init(int x, int y, PaddleScript paddle) {
		this.paddle = paddle;
		this.x = x;
		this.y = y;
	}

	// Update is called once per frame
	void Update() {

	}

	void OnCollisionEnter2D(Collision2D collision) {
		paddle.OnPixelHit(this, x, y);
	}
}
