﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

	public GameScript game;
	private Vector3 initialPosition;
	private Rigidbody2D rigidBody2d;
	private AudioSource audioSource;

	public AudioClip hitWallSound,
		hitPaddleSound;

	public float ballSpeed = 40f;

	// Use this for initialization
	void Awake() {
		initialPosition = transform.position;
		rigidBody2d = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.name.Equals("Left Edge")) {
			// Right player gains a point
			game.RightPlayerScored();
		} else if (collider.gameObject.name.Equals("Right Edge")) {
			// Left player gains a point
			game.LeftPlayerScored();
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Wall") {
			audioSource.PlayOneShot(hitWallSound);
		} else if (collision.gameObject.tag == "Player") {
			audioSource.PlayOneShot(hitPaddleSound);
		}
	}

	public void OnGameStart() {
		gameObject.SetActive(true);
	}

	public void OnGameOver() {
		gameObject.SetActive(false);
	}

	public void Restart() {
		StartCoroutine(startRound());
	}

	private IEnumerator startRound() {
		transform.position = initialPosition;
		rigidBody2d.velocity = new Vector3();

		yield return new WaitForSeconds(0.5f);

		float angle = Random.Range(30f, 60f) // Random angle
			+ (int)Random.Range(0, 4) * 90; // Random quadrant
		rigidBody2d.velocity = Quaternion.Euler(0, 0, angle) * new Vector2(ballSpeed, 0);
	}
}
